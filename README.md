Bit of a mess, sorry!


bug: going right, press up and then left before up happens, you
     crash into tail!
possible solution: a queue of 2 legal (check!) moves.

bug: head crashes into the tail-tip before it has moved. (easiest to
     see in wrap-around mode)

replays work:
(lispbuilder-sdl-worm:play)   ; (setf *machine* (worm:make-machine))
(lispbuilder-sdl-worm:replay) ; replays the history of current *machine*
