;; TODO: add a renderer for /images that you can toggle mid-game.
(ql:quickload :worm)
(ql:quickload :lispbuilder-sdl)
(in-package #:lispbuilder-sdl-worm)

(defparameter *machine* (worm:make-machine))

(defparameter *grid-size* (list 16 16))
(defparameter *renderers* nil)

;; this should be more general, to be used by all frameworks.
(defparameter *theme* `(:snake      (:color ,sdl:*green*)
                        :snake-head (:color ,sdl:*yellow*)
                        :apple      (:color ,sdl:*red*)))

(defun render-object/boxes (o)
  (let ((w (first  *grid-size*))
        (h (second *grid-size*))
        (x (first  (getf o :position)))
        (y (second (getf o :position)))
        (color (getf (getf *theme* (getf o :type))
                     :color)))
    (sdl:draw-box-* (*  w x)
                    (*  h y)
                    (1- w)
                    (1- h) :color color)))

(defmethod render/boxes ((o worm:machine))
  (mapcar #'render-object/boxes (worm:graphical-objects o)))

(push #'render/boxes *renderers*)

(defmethod render ((o worm:machine))
  (funcall (first *renderers*) o))

(defun sdl-key->snes (key)
  (getf '(:sdl-key-up :up
          :sdl-key-down :down
          :sdl-key-left :left
          :sdl-key-right :right)
        key))

(defun start ()
  (worm:init *machine*)
  (sdl:with-init ()
    (sdl:window (* (first  *grid-size*) (first  (worm:size *machine*)))
                (* (second *grid-size*) (second (worm:size *machine*))))
    (setf (sdl:frame-rate) 60)

    (sdl:with-events ()
      (:quit-event () t)
      (:key-down-event (:key key)
        (when (eq :sdl-key-escape key)
          (sdl:push-quit-event))
        (let ((k (sdl-key->snes key)))
          (when k
            (worm:input *machine* (list :press k)))))
      (:key-up-event (:key key)
        (let ((k (sdl-key->snes key)))
          (when k
            (worm:input *machine* (list :release k)))))
      (:idle ()
        (worm:tick *machine*)
        (sdl:clear-display sdl:*black*)
        (render *machine*)
        (sdl:update-display)))))

(defun play ()
  (setf *machine* (worm:make-machine))
  (start)) ; or `re-start'

(defun replay ()
  (setf (worm:mode *machine*) :replay)
  (start))
