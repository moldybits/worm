(ql:quickload :worm)
(ql:quickload :cl-charms)
(in-package #:cl-charms-worm)

(defun char->snes (key)
  (getf '(#\w :up
          #\s :down
          #\a :left
          #\d :right)
        key))

(defparameter *theme* `(:snake-head (:char #\*)
                        :snake      (:char #\o)
                        :apple      (:char #\@)))

(defun render-object (o)
  (let ((x (first  (getf o :position)))
        (y (second (getf o :position)))
        (c (getf (getf *theme* (getf o :type))
                 :char)))
    (charms:write-char-at-point charms:*standard-window*
                                c
                                x
                                y)))

(defmethod render ((o worm:machine))
  (mapcar #'render-object (worm:graphical-objects o)))

(defun start ()
  (charms:with-curses ()
    (charms:disable-echoing)                                 ; ???
    (charms:enable-raw-input :interpret-control-characters t) ; ???
    (charms:enable-non-blocking-mode charms:*standard-window*) ; ???

    (let ((machine ; does it really return 2 values?
           (multiple-value-bind (width height) (charms:window-dimensions charms:*standard-window*)
             (worm:make-machine :size (list width height)))))
      (worm:init machine)

      (loop :named driver-loop          ; interesting!
            :for c := (charms:get-char charms:*standard-window* ; do you have to specify this? yes
                                       :ignore-error t) ; what errors?
         :do (progn                     ; do you need this?
               (case c
                 ((#\q #\Q) (return-from driver-loop))
                 (t (and (char->snes c) (worm:input machine (list :press (char->snes c))))))

               (worm:tick machine)

               (charms:clear-window charms:*standard-window*)
               (render machine)
               (charms:refresh-window charms:*standard-window*)
               (sleep 1/60))))))

(start)
