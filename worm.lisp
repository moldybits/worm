;; TODO
;; save/load (state-save/load)
;; n-player version!
;; perhaps "machine" -> "game" or "engine"
;; give ai what it needs? (to "see", etc)

(in-package :worm)

;; buttons used by machine, specified so that we know what to bind to.
(defparameter *buttons* `(:left :right :up :down)) ; :reset :power :a :b :x :y :start :select

(defclass machine ()
  ((size          :accessor size          :initarg :size          :initform (list 16 16))
   (snake         :accessor snake         :initarg :snake         :initform (list (list 0 0)))
   (direction     :accessor direction     :initarg :snake         :initform '(0 1))
   (apple         :accessor apple         :initarg :apple         :initform nil) ; must be initialized
   (frame         :accessor frame         :initarg :frame         :initform 0)
   (game-over-p   :accessor game-over-p   :initarg :game-over-p   :initform nil)
   (history       :accessor history       :initarg :history       :initform nil) ; or `replay'
   (seed          :accessor seed          :initarg :seed          :initform nil)
   (mode          :accessor mode          :initarg :mode          :initform :play)
   (wrap-around-p :accessor wrap-around-p :initarg :wrap-around-p :initform t)
   (cooldown      :accessor cooldown      :initarg :cooldown      :initform nil)
   (new-cooldown  :accessor new-cooldown  :initarg :new-cooldown  :initform (lambda (snake-length)
                                                                           (min 45 (/ 60 snake-length))))))

(defmethod empty-tile-p ((o machine) x y)
  (not (member (list x y)
             (cons (apple o) (snake o)) ; maybe (board-objects o)
             :test #'equal)))

(defmethod random-empty-tile ((o machine))
  (let ((x (random (first  (size o))))
        (y (random (second (size o)))))
    (if (empty-tile-p o x y)
        (list x y)
        (random-empty-tile o))))

(defmethod graphical-objects ((o machine))
  (append (list (list :type :apple :position (apple o)))
          (mapcar (lambda (x) (list :type :snake :position x))
                  (rest (snake o)))
          (list (list :type :snake-head :position (first (snake o))))))

(defun make-machine (&key (size (list 16 16)))
  (make-instance 'machine :size size :seed (make-random-state t)))

;; this is like pressing the reset button. `seed' and `history' are kept!
(defmethod init ((o machine))
  (setf *random-state* (make-random-state (or (seed o) t))) ; nil would work too, but ...
  (setf (snake o) (list (list 0 0)))   ; can you access the default values?
  (setf (direction o) '(0 1))
  (setf (apple o) (list (random (first  (size o)))
                        (random (second (size o)))))
  (setf (frame o) 0)
  (setf (game-over-p o) nil)
  (setf (cooldown o) (funcall (new-cooldown o) (length (snake o)))))

(defmethod tick ((o machine))
  (when (game-over-p o)
    (return-from tick))

  (incf (frame o))

  (when (<= (decf (cooldown o)) 0)
    (move o)
    (setf (cooldown o)
          (funcall (new-cooldown o) (length (snake o)))))) ; maybe even less specific, and give it machine.

(defmethod move ((o machine))
  (let* ((old-head (first (snake o)))
         (new-head (if (wrap-around-p o)
                       (list (mod (+ (first  old-head) (first  (direction o)))
                                  (first (size o)))
                             (mod (+ (second old-head) (second (direction o)))
                                  (second (size o))))
                       (list (+ (first  old-head) (first  (direction o)))
                             (+ (second old-head) (second (direction o)))))))
    ;; check border collision
    (when (and (not (wrap-around-p o))
               (not (and (< -1 (first new-head)  (first (size o)))
                       (< -1 (second new-head) (second (size o))))))
      (setf (game-over-p o) 'border-collision)
      (return-from move))
    
    ;; check body collision
    (when (find new-head (rest (snake o)) :test #'equal)
      (setf (game-over-p o) 'body-collision)
      (return-from move))

    (push new-head (snake o))
    
    ;; check apple collision
    (if (equal new-head (apple o))
        (setf (apple o) (random-empty-tile o))
        (setf (snake o) (butlast (snake o)))))) ; only remove tail if no apple eaten

;; input all the events from `history' for this frame.
(defmethod tick :before ((o machine))
  (when (string-equal :replay (mode o))
    (loop :while (and (history o)
                      (= (frame o)
                         (first (first (history o)))))
       :do (input o (second (pop (history o)))))))

(defmethod input ((o machine) event)
  (when (and (eq :press (first event))
             (legal-move-p o event))

    (when (string-equal :play (mode o))
      (setf (history o)       ; necessary for when (history o) is NIL.
            (nconc (history o)
                   (list (list (frame o) event)))))
    
    (case (second event)
      (:up    (setf (direction o) '(0 -1)))
      (:down  (setf (direction o) '(0  1)))
      (:right (setf (direction o) '( 1 0)))
      (:left  (setf (direction o) '(-1 0))))))

(defun legal-move-p (o event)
  (or (and (member (second event) '(:up :down))
           (member (direction o) '((1 0) (-1 0)) :test #'equal))
      (and (member (second event) '(:left :right))
           (member (direction o) '((0 1) (0 -1)) :test #'equal))))

;; (event worm-machine '(:tick))
;; (event worm-machine '(:press :left))
;; keep?
(defmethod event ((o machine) event)
  (case (first event)
    (:tick (tick o))
    (t     (input o event))))

