(defpackage #:worm
  (:use cl)
  (:export tick
           input
           init
           machine ; to allow specialization
           make-machine
           size
           snake
           direction
           apple
           frame
           game-over-p
           history
           seed
           mode
           wrap-around-p
           cooldown
           new-cooldown
           graphical-objects
           ))

(defpackage #:trivial-gamekit-worm
  (:use cl)
  (:export
   start
   stop))

(defpackage #:lispbuilder-sdl-worm
  (:use cl)
  (:export
   play
   replay))

(defpackage #:cl-charms-worm
  (:use cl)
  (:export
   start))
