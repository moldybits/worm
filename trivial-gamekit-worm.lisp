(ql:quickload :worm)
(ql:quickload :trivial-gamekit)
(in-package #:trivial-gamekit-worm)

;;------------------------------------------------------------------------------
;; Defining Global Variables/Constants
;;------------------------------------------------------------------------------
;; Window Paramters
(defparameter *tile-size*    32)
(defparameter *board-width*  16)
(defparameter *board-height* 9)
(defparameter *canvas-width*  (* *board-width*   *tile-size*))
(defparameter *canvas-height* (* *board-height*  *tile-size*))

;; Position Constants
(defparameter *origin*              (gamekit:vec2 0 0))
(defparameter *bottom-left-corner*  (gamekit:vec2 0 0))
(defparameter *top-left-corner*     (gamekit:vec2 0 *canvas-height*))
(defparameter *bottom-right-corner* (gamekit:vec2 *canvas-height* 0))
(defparameter *top-right-corner*    (gamekit:vec2 *canvas-width* *canvas-height*))
(defparameter *center*              (gamekit:vec2 (/ *canvas-width* 2)
                                                  (/ *canvas-height* 2)))

;; Movement Vectors
(defparameter *mov-up-vec*    (gamekit:vec2  0    10))
(defparameter *mov-down-vec*  (gamekit:vec2  0   -10))
(defparameter *mov-left-vec*  (gamekit:vec2 -10   0 ))
(defparameter *mov-right-vec* (gamekit:vec2  10   0 ))

;; Colors Constants
(defvar +black+ (gamekit:vec4 0 0 0 1))
(defvar +white+ (gamekit:vec4 1 1 1 1))
(defvar +red+   (gamekit:vec4 1 0 0 1))
(defvar +green+ (gamekit:vec4 0 1 0 1))
(defvar +blue+  (gamekit:vec4 0 0 1 1))

;; Other Variables
(defparameter *random-text-1* "random-text-1")
(defparameter *random-text-pos-1* *center*)
(defparameter *random-text-2* "random-text-2")
(defparameter *random-text-pos-2* *center*)

;;------------------------------------------------------------------------------
;; Define Game
;------------------------------------------------------------------------------
(gamekit:defgame worm () ()
                 (:viewport-width (* *canvas-width* 2))
                 (:viewport-height (* *canvas-height* 2))
                 (:viewport-title "trivial-gamekit-worm")
                 (:prepare-resources t))

;;------------------------------------------------------------------------------
;; Defining Global Resources
;;------------------------------------------------------------------------------
(gamekit:register-resource-package
 :keyword (asdf:system-relative-pathname :worm "assets/"))

;; Sounds
(gamekit:define-sound :swish-30 "snd/misc/Swish30-Single.wav")

;; Fonts
(gamekit:define-font :cleon-light  "font/Cleon-Light.ttf")
(gamekit:define-font :black-animal "font/Black-Animal.ttf")

;; Sprites
(gamekit:define-image :background-1 "img/background-1.png")
(gamekit:define-image :background-2 "img/background-2.png")
(gamekit:define-image :grid         "img/grid.png")
(gamekit:define-image :0            "img/0.png")
(gamekit:define-image :1            "img/1.png")
(gamekit:define-image :2            "img/2.png")
(gamekit:define-image :3            "img/3.png")

;;------------------------------------------------------------------------------
;; post init
;;------------------------------------------------------------------------------
(defmethod gamekit:post-initialize ((app worm))
  ;; (gamekit:bind-cursor (lambda (x y)
  ;;                        ))
  )

;;------------------------------------------------------------------------------
;; utility functions
;;------------------------------------------------------------------------------
(defun real-time-seconds ()
  "Return seconds since certain point of time"
  (/ (get-internal-real-time) internal-time-units-per-second))

(defun update-position-jump (position time)
  "Update position vector depending on the time supplied
  HERE: jumping"
  (let* ((subsecond (nth-value 1 (truncate time)))
         (angle (* 2 pi subsecond)))
    (setf (gamekit:y position) (+ 300 (* 100 (sin angle))))))


(defun update-position-circle (position time)
  "Update position vector depending on the time supplied
  HERE: circular movement"
  (let* ((subsecond (nth-value 1 (truncate time)))
         (angle (* 2 pi subsecond)))
    (setf (gamekit:x position) (+ (* (cos angle) 150) (/ *canvas-width* 6))
          (gamekit:y position) (+ (* (sin angle) 90) (/ *canvas-height* 4)))))

(defun grey-shade (&key (value 0)  (opacity 1))
  "Value and opacity need to be between 0 and 1."
  (gamekit:vec4 value value value opacity))

;;------------------------------------------------------------------------------
;; Every Frame Calculations
;;------------------------------------------------------------------------------
(defmethod gamekit:act ((app worm))
  (update-position-circle *random-text-pos-1* (/ (real-time-seconds) 20))
  (update-position-circle *random-text-pos-2* (/ (real-time-seconds) 5))
  )



;;------------------------------------------------------------------------------
;; Every Frame Drawing
;;------------------------------------------------------------------------------
(defmethod gamekit:draw ((app worm))
  ;; draw background
  (gamekit:with-pushed-canvas ()
    (gamekit:scale-canvas 2 2)
    (gamekit:draw-image *origin* :background-1))

  (gamekit:with-pushed-canvas ()
    (gamekit:scale-canvas 1.9 1.9)
    (gamekit:draw-text *random-text-1*
                       *random-text-pos-1*
                       :fill-color (grey-shade :value 0)
                       :font (gamekit:make-font :cleon-light 32)))
  (gamekit:with-pushed-canvas ()
    (gamekit:scale-canvas 2 2)
    (gamekit:draw-text *random-text-2*
                       *random-text-pos-2*
                       :fill-color (grey-shade :value 1)
                       :font (gamekit:make-font :cleon-light 32)))


  )

;;------------------------------------------------------------------------------
;; Exported Functions
;;------------------------------------------------------------------------------
(defun start ()
  (gamekit:start 'worm))

(defun stop ()
  (gamekit:stop))
